FROM eclipse-temurin:21

RUN mkdir -p /app && apt update && apt install -y ffmpeg wget
WORKDIR /app
COPY target/CatboatSinger.jar .
COPY yt-dlp .
RUN chmod +x yt-dlp

CMD ["java", "-jar", "CatboatSinger.jar"]
