package fr.melaine_gerard.catboat.utils;

public class Constants {
  private static final Config config = Config.getInstance();
  
  public static final String BOT_TOKEN = config.get("BOT_TOKEN");
  
  public static final String BOT_OWNER = config.get("BOT_OWNER_ID");
  
  public static final String GUILD_ID = config.get("BOT_GUILD_ID");

  public static final String PLAYING_MESSAGE = config.get("BOT_PLAYING_MESSAGE");
  public static final String SPOTIFY_ID = config.get("SPOTIFY_CLIENT_ID");
  public static final String SPOTIFY_SECRET = config.get("SPOTIFY_CLIENT_SECRET");
  public static final String MINIO_URL = config.get("MINIO_URL");
  public static final String MINIO_USERNAME = config.get("MINIO_USERNAME");
  public static final String MINIO_PASSWORD = config.get("MINIO_PASSWORD");
}
