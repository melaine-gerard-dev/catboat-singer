package fr.melaine_gerard.catboat.utils;

import io.minio.*;
import io.minio.errors.*;
import io.minio.http.Method;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class YoutubeDownload {
    private static final Logger logger = LoggerFactory.getLogger(YoutubeDownload.class);
    private static BufferedReader runYoutubeDl(String link, File audioDir) throws IOException {
        ProcessBuilder processBuilder = new ProcessBuilder(
                "./yt-dlp",
                "--cookies", "./audio/cookies.txt",
                "-x",
                "--audio-format", "mp3",
                "-P", audioDir.getAbsolutePath(),
                "--output", "%(title)s.%(ext)s",
                link
        );
        Process process = processBuilder.start();
        return new BufferedReader(new InputStreamReader(process.getInputStream()));
    }

    public static List<String> downloadFilesFromYoutubeDl(String link) throws IOException, ServerException, InsufficientDataException, ErrorResponseException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        List<String> urls = new ArrayList<>();
        MinioClient minioClient = MinioClient.builder()
                .endpoint(Constants.MINIO_URL)
                .credentials(Constants.MINIO_USERNAME, Constants.MINIO_PASSWORD)
                .build();

        logger.info("Connected to minio");

        boolean found =
                minioClient.bucketExists(BucketExistsArgs.builder().bucket("audio").build());
        if (!found) {
            minioClient.makeBucket(MakeBucketArgs.builder().bucket("audio").build());
        }
        logger.info("Bucket ready");

        File audioDir = new File("audio");
        audioDir.mkdirs();
        logger.info("Folder Ready");

        BufferedReader read = YoutubeDownload.runYoutubeDl(link, audioDir);
        for (String line; (line = read.readLine()) != null; ) {
            logger.info(line);
        }
        logger.info("File downloaded");

        Set<String> fileList = listFiles(audioDir.getAbsolutePath());

        for (String filename : fileList) {
            logger.info("Uploading {}", filename);
            minioClient.uploadObject(
                    UploadObjectArgs.builder()
                            .bucket("audio")
                            .object(filename)
                            .filename(new File(audioDir, filename).getAbsolutePath())
                            .build());
            logger.info("Uploaded {}", filename);
            String uri = minioClient.getPresignedObjectUrl(
                    GetPresignedObjectUrlArgs
                            .builder()
                            .bucket("audio")
                            .object(filename)
                            .expiry(1, TimeUnit.DAYS)
                            .method(Method.GET)
                            .build()
            );

            logger.info("Generated uri : {}", uri);
            urls.add(uri);

            Files.deleteIfExists(new File(audioDir, filename).toPath());
            logger.info("Deleted file : {}", filename);
        }

        read.close();

        return urls;
    }



    private static Set<String> listFiles(String dir) {
        return Stream.of(Objects.requireNonNull(new File(dir).listFiles()))
                .filter(file -> !file.isDirectory())
                .filter(file -> file.getName().endsWith(".mp3"))
                .map(File::getName)
                .collect(Collectors.toSet());
    }
}
