package fr.melaine_gerard.catboat.utils;

import io.github.cdimascio.dotenv.Dotenv;

public class Config {
  private static Config instance;
  
  private final Dotenv dotenv;
  
  public static Config getInstance() {
    if (instance == null)
      instance = new Config(); 
    return instance;
  }
  
  public Config() {
    this.dotenv = Dotenv.configure().ignoreIfMissing().ignoreIfMalformed().load();
  }

  public String get(String key) {
    return this.dotenv.get(key, "");
  }

}
