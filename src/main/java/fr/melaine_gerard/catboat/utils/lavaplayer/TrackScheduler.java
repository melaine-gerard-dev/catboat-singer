package fr.melaine_gerard.catboat.utils.lavaplayer;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason;
import fr.melaine_gerard.catboat.core.BotCore;
import fr.melaine_gerard.catboat.utils.Constants;

import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class TrackScheduler extends AudioEventAdapter {
    public final AudioPlayer player;

    public final BlockingQueue<AudioTrack> queue;

    public boolean repeating = false;

    public TrackScheduler(AudioPlayer player) {
        this.player = player;
        this.queue = new LinkedBlockingQueue<>();
    }

    public void onTrackEnd(AudioPlayer player, AudioTrack track, AudioTrackEndReason endReason) {
        if (endReason.mayStartNext) {
            if (this.repeating) {
                (PlayerManager.getInstance().getMusicManager(Objects.requireNonNull(BotCore.getInstance().getJda().getGuildById(Constants.GUILD_ID)))).scheduler.queue(track.makeClone());
                return;
            }
            nextTrack();
        }
    }

    public void nextTrack() {
        AudioTrack poll = this.queue.poll();
        if (poll == null)
            Objects.requireNonNull(BotCore.getInstance().getJda().getGuildById(Constants.GUILD_ID)).getAudioManager().closeAudioConnection();
        this.player.startTrack(poll, false);
    }

    public void queue(AudioTrack track) {
        if (!this.player.startTrack(track, true))
            this.queue.offer(track);
    }
}
