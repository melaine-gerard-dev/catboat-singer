package fr.melaine_gerard.catboat.utils.lavaplayer;

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dev.lavalink.youtube.YoutubeAudioSourceManager;
import dev.lavalink.youtube.clients.*;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;

public class PlayerManager {
    private static PlayerManager INSTANCE;

    private final Map<Long, GuildMusicManager> musicManagers;

    private final AudioPlayerManager audioPlayerManager;

    public static PlayerManager getInstance() {
        if (INSTANCE == null)
            INSTANCE = new PlayerManager();
        return INSTANCE;
    }

    public PlayerManager() {
        this.musicManagers = new HashMap<>();
        this.audioPlayerManager = new DefaultAudioPlayerManager();
        YoutubeAudioSourceManager youtubeAudioSourceManager = new YoutubeAudioSourceManager(true, true, true, new AndroidMusic(), new AndroidLite(), new Music(), new TvHtml5Embedded(), new MediaConnect(), new Ios(), new Web());
        // On ajoute l'authentification à Youtube
        this.audioPlayerManager.registerSourceManager(youtubeAudioSourceManager);
        AudioSourceManagers.registerRemoteSources(this.audioPlayerManager);
        AudioSourceManagers.registerLocalSource(this.audioPlayerManager);
    }

    public GuildMusicManager getMusicManager(Guild guild) {
        return this.musicManagers.computeIfAbsent(guild.getIdLong(), guildId -> {
            GuildMusicManager guildMusicManager = new GuildMusicManager(this.audioPlayerManager);
            guild.getAudioManager().setSendingHandler(guildMusicManager.getSendHandler());
            return guildMusicManager;
        });
    }

    public void loadAndPlay(final TextChannel channel, String trackUrl, String baseString) {
        final GuildMusicManager musicManager = getMusicManager(channel.getGuild());
        this.audioPlayerManager.loadItemOrdered(musicManager, trackUrl, new AudioLoadResultHandler() {
            public void trackLoaded(AudioTrack audioTrack) {
                musicManager.scheduler.queue(audioTrack);

                if (baseString.contains("youtube.com") || baseString.contains("ytsearch1:")) {
                    return;
                }

                String sb = "Ajout dans la file d'attente : `" +
                        (audioTrack.getInfo()).title +
                        "` by `" +
                        (audioTrack.getInfo()).author +
                        "`";

                channel.sendMessage(sb)
                        .queue();
            }

            public void playlistLoaded(AudioPlaylist audioPlaylist) {
                List<AudioTrack> tracks = audioPlaylist.getTracks();
                if (audioPlaylist.isSearchResult()) {
                    AudioTrack audioTrack = tracks.getFirst();
                    musicManager.scheduler.queue(audioTrack);

                    String sb = "Ajout dans la file d'attente : `" +
                            (audioTrack.getInfo()).title +
                            "` by `" +
                            (audioTrack.getInfo()).author +
                            "`";

                    channel.sendMessage(sb)
                            .queue();
                } else {

                    String sb = "Ajout dans la file d'attente : `" +
                            tracks.size() +
                            "` musiques de la playlist `" +
                            audioPlaylist.getName() +
                            "`";

                    channel.sendMessage(sb)
                            .queue();
                    for (AudioTrack track : tracks)
                        musicManager.scheduler.queue(track);
                }
            }

            public void noMatches() {
                channel.sendMessage("Aucune musique trouvé !")
                        .queue();
            }

            public void loadFailed(FriendlyException e) {
                channel.sendMessage("Impossible de charger cette musique : " + e.getMessage())
                        .queue();
            }
        });
    }
}
