package fr.melaine_gerard.catboat.utils;

import org.apache.hc.core5.http.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.michaelthelin.spotify.SpotifyApi;
import se.michaelthelin.spotify.exceptions.SpotifyWebApiException;
import se.michaelthelin.spotify.model_objects.credentials.ClientCredentials;
import se.michaelthelin.spotify.model_objects.specification.ArtistSimplified;
import se.michaelthelin.spotify.model_objects.specification.Track;
import se.michaelthelin.spotify.requests.authorization.client_credentials.ClientCredentialsRequest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SpotifyHelper {
    private static SpotifyApi spotifyApi;
    private static SpotifyApi getSpotifyApi() {
        if (spotifyApi == null) {
            spotifyApi = new SpotifyApi.Builder()
                    .setClientId(Constants.SPOTIFY_ID)
                    .setClientSecret(Constants.SPOTIFY_SECRET)
                    .build();

            ClientCredentialsRequest clientCredentialsRequest = spotifyApi.clientCredentials().build();

            try {
                ClientCredentials clientCredentials = clientCredentialsRequest.execute();
                spotifyApi.setAccessToken(clientCredentials.getAccessToken());
            } catch (IOException | SpotifyWebApiException | ParseException e) {
                throw new RuntimeException(e);
            }
        }

        return spotifyApi;
    }

    public static String getTrackId(String url) {
        String realUrl = url.split("\\?")[0];

        String[] urlParts = realUrl.split("/");

        return urlParts[urlParts.length - 1];
    }

    public static Track getTrackInformations(String trackId) {
        try {

            return getSpotifyApi().getTrack(trackId).build().execute();

        } catch (IOException | ParseException | SpotifyWebApiException e) {
            throw new RuntimeException(e);
        }
    }

    public static String buildTrackInformations(String trackId) {
        Track track = getTrackInformations(trackId);
        List<String> artistNames = new ArrayList<>();

        for (ArtistSimplified artistSimplified : track.getArtists()) {
            artistNames.add(artistSimplified.getName());
        }

        return "%s - %s - %s".formatted(
                track.getName(),
                String.join(", ", artistNames),
                track.getAlbum().getName()
        );
    }
}
