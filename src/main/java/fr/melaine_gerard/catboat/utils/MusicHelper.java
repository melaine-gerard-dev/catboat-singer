package fr.melaine_gerard.catboat.utils;

import fr.melaine_gerard.catboat.utils.lavaplayer.PlayerManager;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.GuildVoiceState;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.channel.middleman.AudioChannel;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.managers.AudioManager;

import java.net.MalformedURLException;
import java.net.URL;

public class MusicHelper {
    public static void joinChannel(AudioManager audioManager, AudioChannel voiceChannel) {
        audioManager.openAudioConnection(voiceChannel);
        (PlayerManager.getInstance().getMusicManager(voiceChannel.getGuild())).audioPlayer.setVolume(20);
    }

    public static void leaveChannel(AudioManager audioManager) {
        audioManager.closeAudioConnection();
    }

    public static boolean isInVoiceChannel(GuildVoiceState guildVoiceState) {
        return guildVoiceState.inAudioChannel();
    }

    public static boolean inTheSameChannel(String firstChannelId, String secondChannelId) {
        return firstChannelId.equals(secondChannelId);
    }

    public static boolean isUrl(String url) {
        try {
            new URL(url);
            return true;
        } catch (MalformedURLException e) {
            return false;
        }
    }

    public static boolean checksForMusicPart(SlashCommandInteractionEvent event) {
        Guild guild = event.getGuild();
        if (guild == null) {
            event.reply("Tu dois utiliser cette commande sur un serveur !").queue();
            return false;
        }
        Member selfMember = guild.getSelfMember();
        Member member = event.getMember();
        if (member == null) {
            event.reply("Impossible de récupérer ton profil !").queue();
            return false;
        }
        GuildVoiceState memberVoiceState = member.getVoiceState();
        if (memberVoiceState == null || !isInVoiceChannel(memberVoiceState)) {
            event.reply("Tu dois être dans un salon vocal !").queue();
            return false;
        }
        AudioManager audioManager = guild.getAudioManager();
        GuildVoiceState selfMemberVoiceState = selfMember.getVoiceState();
        if (selfMemberVoiceState == null || !isInVoiceChannel(selfMemberVoiceState)) {
            AudioChannel audioChannel = memberVoiceState.getChannel();
            if (audioChannel == null) {
                event.reply("Impossible de récupérer ton salon vocal !").queue();
                return false;
            }
            if (!selfMember.hasPermission(audioChannel, Permission.VOICE_CONNECT)) {
                event.reply("Je ne peux pas rejoindre ce salon !").queue();
                return false;
            }
            joinChannel(audioManager, audioChannel);
        } else {
            // Check si le bot est dans le même salon que le membre
            AudioChannel audioChannel = memberVoiceState.getChannel();
            if (audioChannel == null) {
                event.reply("Impossible de récupérer ton salon vocal !").queue();
                return false;
            }

            AudioChannel selfMemberAudioChannel = selfMemberVoiceState.getChannel();
            if (selfMemberAudioChannel == null) {
                event.reply("Impossible de récupérer mon salon vocal !").queue();
                return false;
            }

            if (!inTheSameChannel(audioChannel.getId(), selfMemberAudioChannel.getId())) {
                event.reply("Tu dois être dans le même salon que moi !").queue();
                return false;
            }
        }
        return true;
    }
}
