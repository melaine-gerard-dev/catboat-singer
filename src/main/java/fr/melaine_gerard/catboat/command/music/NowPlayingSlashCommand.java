package fr.melaine_gerard.catboat.command.music;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import fr.melaine_gerard.catboat.command.ISlashCommand;
import fr.melaine_gerard.catboat.utils.MusicHelper;
import fr.melaine_gerard.catboat.utils.lavaplayer.PlayerManager;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import java.time.Instant;
import java.util.Objects;

public class NowPlayingSlashCommand implements ISlashCommand {

    public String getDescription() {
        return "Permet de voir la musique en cours de lecture";
    }

    public void handle(SlashCommandInteractionEvent event) {
        if (!MusicHelper.checksForMusicPart(event))
            return;
        AudioTrack playingTrack = (PlayerManager.getInstance().getMusicManager(Objects.requireNonNull(event.getGuild()))).scheduler.player.getPlayingTrack();
        EmbedBuilder embedBuilder = new EmbedBuilder();
        embedBuilder.setTitle("Musique actuelle");
        embedBuilder.setFooter(event.getJDA().getSelfUser().getName(), event.getJDA().getSelfUser().getAvatarUrl());
        embedBuilder.setTimestamp(Instant.now());
        if (playingTrack == null) {
            embedBuilder.setDescription("Aucune musique en cours de lecture");
        } else {
            embedBuilder.addField("Titre", (playingTrack.getInfo()).title, false);
            embedBuilder.addField("Auteur", (playingTrack.getInfo()).author, false);
            embedBuilder.addField("URL", (playingTrack.getInfo()).uri, false);
        }
        event.replyEmbeds(embedBuilder.build(), new net.dv8tion.jda.api.entities.MessageEmbed[0]).queue();
    }
}
