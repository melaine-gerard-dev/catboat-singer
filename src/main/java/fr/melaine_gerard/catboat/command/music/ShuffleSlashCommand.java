package fr.melaine_gerard.catboat.command.music;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import fr.melaine_gerard.catboat.command.ISlashCommand;
import fr.melaine_gerard.catboat.utils.MusicHelper;
import fr.melaine_gerard.catboat.utils.lavaplayer.GuildMusicManager;
import fr.melaine_gerard.catboat.utils.lavaplayer.PlayerManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

public class ShuffleSlashCommand implements ISlashCommand {
  public String getDescription() {
    return "Permet de mélanger l'ordre des musiques dans la liste de lecture";
  }
  
  public void handle(SlashCommandInteractionEvent event) {
    MusicHelper.checksForMusicPart(event);
    Guild guild = event.getGuild();
    assert guild != null;
    GuildMusicManager musicManager = PlayerManager.getInstance().getMusicManager(guild);
    List<AudioTrack> tracks = new ArrayList<>(musicManager.scheduler.queue);
    Collections.shuffle(tracks);
    musicManager.scheduler.queue.clear();
    musicManager.scheduler.queue.addAll(tracks);
    event.reply("La liste de lecture a été mélangée").queue();
  }
}
