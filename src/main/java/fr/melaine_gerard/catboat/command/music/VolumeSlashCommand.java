package fr.melaine_gerard.catboat.command.music;

import fr.melaine_gerard.catboat.command.ISlashCommand;
import fr.melaine_gerard.catboat.utils.MusicHelper;
import fr.melaine_gerard.catboat.utils.lavaplayer.PlayerManager;
import java.util.Collections;
import java.util.List;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public class VolumeSlashCommand implements ISlashCommand {
  public String getDescription() {
    return "Permet de gérer le volume du bot";
  }
  
  public void handle(SlashCommandInteractionEvent event) {
    if (event.getOptions().isEmpty()) {
      event.reply("Merci d'indiquer le volume souhaité !").queue();
      return;
    } 
    if (!MusicHelper.checksForMusicPart(event))
      return; 
    Guild guild = event.getGuild();
    OptionMapping volume = event.getOption("volume");
    if (volume == null) {
      event.reply("Impossible de récupérer le volume proposé !").queue();
      return;
    } 
    int volumeAsInt = volume.getAsInt();
    if (volumeAsInt < 0 || volumeAsInt > 100) {
      event.reply("Le volume doit être entre 0 et 100 %").queue();
      return;
    } 
    assert guild != null;
    (PlayerManager.getInstance().getMusicManager(guild)).audioPlayer.setVolume(volumeAsInt);
    event.reply("Passage du volume à " + volumeAsInt + "% !").queue();
  }
  
  public List<OptionData> getOptions() {
    return Collections.singletonList((new OptionData(OptionType.INTEGER, "volume", "Le pourcentage de volume souhaité", true)).setRequiredRange(0L, 100L));
  }
}
