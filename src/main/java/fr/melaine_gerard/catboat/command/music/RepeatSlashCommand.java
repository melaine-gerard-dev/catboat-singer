package fr.melaine_gerard.catboat.command.music;

import fr.melaine_gerard.catboat.command.ISlashCommand;
import fr.melaine_gerard.catboat.utils.MusicHelper;
import fr.melaine_gerard.catboat.utils.lavaplayer.GuildMusicManager;
import fr.melaine_gerard.catboat.utils.lavaplayer.PlayerManager;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

public class RepeatSlashCommand implements ISlashCommand {
  public String getDescription() {
    return "Permet de  configurer la répétition des musiques";
  }
  
  public void handle(SlashCommandInteractionEvent event) {
    MusicHelper.checksForMusicPart(event);
    Guild guild = event.getGuild();
    assert guild != null;
    GuildMusicManager musicManager = PlayerManager.getInstance().getMusicManager(guild);
    musicManager.scheduler.repeating = !musicManager.scheduler.repeating;
    if (musicManager.scheduler.repeating) {
      event.reply("La répétition est activée").queue();
    } else {
      event.reply("La répétition est désactivée").queue();
    } 
  }
}
