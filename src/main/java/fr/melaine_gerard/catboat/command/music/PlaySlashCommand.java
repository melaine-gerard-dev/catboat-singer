package fr.melaine_gerard.catboat.command.music;

import fr.melaine_gerard.catboat.command.ISlashCommand;
import fr.melaine_gerard.catboat.utils.MusicHelper;
import fr.melaine_gerard.catboat.utils.SpotifyHelper;
import fr.melaine_gerard.catboat.utils.YoutubeDownload;
import fr.melaine_gerard.catboat.utils.lavaplayer.PlayerManager;
import io.minio.errors.*;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.List;

public class PlaySlashCommand implements ISlashCommand {
    private static final Logger log = LoggerFactory.getLogger(PlaySlashCommand.class);

    public String getDescription() {
        return "Permet d'ajouter une nouvelle musique dans la liste de lecture";
    }

    public List<OptionData> getOptions() {
        return Collections.singletonList((new OptionData(OptionType.STRING, "music", "Le lien ou le nom de la musique")).setRequired(true));
    }

    public void handle(SlashCommandInteractionEvent event) {
        if (event.getOptions().isEmpty()) {
            event.reply("Merci d'indiquer la musique souhaité !").queue();
            return;
        }
        if (!MusicHelper.checksForMusicPart(event)) return;
        OptionMapping music = event.getOption("music");
        if (music == null) {
            event.reply("Impossible de récupérer la musique demandé !").queue();
            return;
        }
        String musicAsString = music.getAsString();
        String url = musicAsString;
        if (!MusicHelper.isUrl(url)) url = "ytsearch1:" + musicAsString;

        if (url.contains("spotify.com") && url.contains("track")) {
            log.info(url);
            String trackId = SpotifyHelper.getTrackId(url);
            String formattedTrackInfos = SpotifyHelper.buildTrackInformations(trackId);

            url = "ytsearch1:" + formattedTrackInfos;
        }

        if (url.contains("youtube.com") || url.contains("ytsearch1:")) {
            event.reply(String.format("Lancement du chargement de votre musique [%s]...", musicAsString)).queue();
            try {
                List<String> urls = YoutubeDownload.downloadFilesFromYoutubeDl(url);

                if (urls.size() == 0) {
                    event.getChannel().sendMessage(String.format("Aucune musique trouvé pour la recherche '%s'", musicAsString)).queue();
                    return;
                }

                for (String ur : urls) {
                    log.info("Loading url : " + ur);
                    PlayerManager.getInstance().loadAndPlay(event.getChannel().asTextChannel(), ur, url);
                }
            } catch (IOException | ServerException | InsufficientDataException | ErrorResponseException |
                    NoSuchAlgorithmException | InvalidKeyException | InvalidResponseException | XmlParserException |
                    InternalException e) {
                event.getChannel().sendMessage(String.format("Une erreur est survenue pendant le chargement de votre musique %s", musicAsString)).queue();
                log.error("An error occurred while downloading music", e);
            }
        } else {
            log.info("Loading music: {}", url);
            PlayerManager.getInstance().loadAndPlay(event.getChannel().asTextChannel(), url, musicAsString);
            event.reply(String.format("Lancement du chargement de votre musique [%s]...", musicAsString)).queue();
        }
    }
}
