package fr.melaine_gerard.catboat.command.music;

import fr.melaine_gerard.catboat.command.ISlashCommand;
import fr.melaine_gerard.catboat.utils.MusicHelper;
import fr.melaine_gerard.catboat.utils.lavaplayer.PlayerManager;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.GuildVoiceState;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.channel.middleman.AudioChannel;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.managers.AudioManager;

public class LeaveSlashCommand implements ISlashCommand {
  public String getDescription() {
    return "Permet de déconnecter le bot du salon";
  }
  
  public void handle(SlashCommandInteractionEvent event) {
    Guild guild = event.getGuild();
    if (guild == null) {
      event.reply("Tu dois utiliser cette commande sur un serveur !").queue();
      return;
    } 
    Member selfMember = guild.getSelfMember();
    GuildVoiceState selfMemberVoiceState = selfMember.getVoiceState();
    if (selfMemberVoiceState == null || !MusicHelper.isInVoiceChannel(selfMemberVoiceState)) {
      event.reply("Je dois être dans un salon vocal !").queue();
      return;
    } 
    Member member = event.getMember();
    if (member == null) {
      event.reply("Impossible de récupérer ton profil !").queue();
      return;
    } 
    GuildVoiceState memberVoiceState = member.getVoiceState();
    if (memberVoiceState == null || !MusicHelper.isInVoiceChannel(memberVoiceState)) {
      event.reply("Tu dois être dans un salon vocal !").queue();
      return;
    } 
    AudioManager audioManager = guild.getAudioManager();
    AudioChannel audioChannel = memberVoiceState.getChannel();
    if (audioChannel == null) {
      event.reply("Impossible de récupérer ton salon vocal !").queue();
      return;
    } 
    if (selfMemberVoiceState.getChannel() == null || !MusicHelper.inTheSameChannel(audioChannel.getId(), selfMemberVoiceState.getChannel().getId())) {
      event.reply("Tu dois être dans le même salon que moi !").queue();
      return;
    } 
    (PlayerManager.getInstance().getMusicManager(event.getGuild())).scheduler.repeating = false;
    (PlayerManager.getInstance().getMusicManager(event.getGuild())).scheduler.queue.clear();
    (PlayerManager.getInstance().getMusicManager(event.getGuild())).scheduler.player.stopTrack();
    MusicHelper.leaveChannel(audioManager);
    event.reply("Déconnexion du salon `🎵 " + audioChannel.getName() + "` en cours...").queue();
  }
}
