package fr.melaine_gerard.catboat.command.music;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import fr.melaine_gerard.catboat.command.ISlashCommand;
import fr.melaine_gerard.catboat.utils.MusicHelper;
import fr.melaine_gerard.catboat.utils.lavaplayer.PlayerManager;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import java.time.Instant;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;

public class QueueSlashCommand implements ISlashCommand {
    public String getDescription() {
        return "Permet de voir les musique dans la liste de lecture";
    }

    public void handle(SlashCommandInteractionEvent event) {
        if (!MusicHelper.checksForMusicPart(event))
            return;
        BlockingQueue<AudioTrack> queue = (PlayerManager.getInstance().getMusicManager(Objects.requireNonNull(event.getGuild()))).scheduler.queue;
        if (queue.isEmpty()) {
            event.reply("La liste de lecture est vide").queue();
        } else {
            EmbedBuilder embedBuilder = new EmbedBuilder();
            embedBuilder.setTitle("Liste de lecture");
            int index = 0;
            for (AudioTrack track : queue) {
                if (index < 10)
                    embedBuilder.addField((track.getInfo()).title, (track.getInfo()).author, false);
                index++;
            }
            if (queue.size() > 10) {
                embedBuilder.setFooter("Et " + (queue.size() - 10) + " autres musiques", event.getJDA().getSelfUser().getAvatarUrl());
            } else {
                embedBuilder.setFooter(event.getJDA().getSelfUser().getName(), event.getJDA().getSelfUser().getAvatarUrl());
            }
            embedBuilder.setTimestamp(Instant.now());
            event.replyEmbeds(embedBuilder.build(), new net.dv8tion.jda.api.entities.MessageEmbed[0]).queue();
        }
    }
}
