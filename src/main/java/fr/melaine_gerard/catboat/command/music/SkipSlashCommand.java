package fr.melaine_gerard.catboat.command.music;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import fr.melaine_gerard.catboat.command.ISlashCommand;
import fr.melaine_gerard.catboat.utils.MusicHelper;
import fr.melaine_gerard.catboat.utils.lavaplayer.GuildMusicManager;
import fr.melaine_gerard.catboat.utils.lavaplayer.PlayerManager;
import java.util.Objects;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

public class SkipSlashCommand implements ISlashCommand {
  public String getDescription() {
    return "Permet de passer la musique en cours de lecture";
  }
  
  public void handle(SlashCommandInteractionEvent event) {
    if (!MusicHelper.checksForMusicPart(event))
      return; 
    GuildMusicManager musicManager = PlayerManager.getInstance().getMusicManager(Objects.requireNonNull(event.getGuild()));
    AudioPlayer audioPlayer = musicManager.audioPlayer;
    if (audioPlayer.getPlayingTrack() == null) {
      event.reply("Impossible de passer la musique, il n'y a pas de musique en cours de lecture !").queue();
      return;
    } 
    musicManager.scheduler.nextTrack();
    event.reply("Passage à la musique suivante...").queue();
  }
}
