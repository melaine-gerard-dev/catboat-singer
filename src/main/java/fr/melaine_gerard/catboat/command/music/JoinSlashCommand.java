package fr.melaine_gerard.catboat.command.music;

import fr.melaine_gerard.catboat.command.ISlashCommand;
import fr.melaine_gerard.catboat.utils.MusicHelper;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.GuildVoiceState;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.channel.middleman.AudioChannel;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.managers.AudioManager;

public class JoinSlashCommand implements ISlashCommand {
  public String getDescription() {
    return "Permet de connecter le bot dans le salon";
  }
  
  public void handle(SlashCommandInteractionEvent event) {
    Guild guild = event.getGuild();
    if (guild == null) {
      event.reply("Tu dois utiliser cette commande sur un serveur !").queue();
      return;
    } 
    Member selfMember = guild.getSelfMember();
    GuildVoiceState selfMemberVoiceState = selfMember.getVoiceState();
    if (selfMemberVoiceState != null && MusicHelper.isInVoiceChannel(selfMemberVoiceState)) {
      event.reply("Je suis déjà connecté dans un autre salon !").queue();
      return;
    } 
    Member member = event.getMember();
    if (member == null) {
      event.reply("Impossible de récupérer ton profil !").queue();
      return;
    } 
    GuildVoiceState memberVoiceState = member.getVoiceState();
    if (memberVoiceState == null || !MusicHelper.isInVoiceChannel(memberVoiceState)) {
      event.reply("Tu dois être dans un salon vocal !").queue();
      return;
    } 
    AudioManager audioManager = guild.getAudioManager();
    AudioChannel audioChannel = memberVoiceState.getChannel();
    if (audioChannel == null) {
      event.reply("Impossible de récupérer ton salon vocal !").queue();
      return;
    } 
    if (!selfMember.hasPermission(audioChannel, Permission.VOICE_CONNECT)) {
      event.reply("Je ne peux pas rejoindre ce salon !").queue();
      return;
    } 
    MusicHelper.joinChannel(audioManager, audioChannel);
    event.reply("Connexion au salon `🎵 " + audioChannel.getName() + "`...").queue();
  }
}
