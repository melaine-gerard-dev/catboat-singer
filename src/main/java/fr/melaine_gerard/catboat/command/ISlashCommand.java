package fr.melaine_gerard.catboat.command;

import java.util.Collections;
import java.util.List;

import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public interface ISlashCommand {
  default String getName() {
    return getClass().getSimpleName().toLowerCase().replaceAll("slashcommand", "");
  }
  
  String getDescription();
  
  default List<OptionData> getOptions() {
    return Collections.emptyList();
  }
  
  default boolean isOwnerCommand() {
    return false;
  }
  
  void handle(SlashCommandInteractionEvent paramSlashCommandInteractionEvent);

  default List<Permission> permissionsNeeded() {
    return Collections.emptyList();
  }
}
