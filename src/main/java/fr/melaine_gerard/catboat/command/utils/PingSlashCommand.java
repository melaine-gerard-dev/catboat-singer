package fr.melaine_gerard.catboat.command.utils;

import fr.melaine_gerard.catboat.command.ISlashCommand;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

public class PingSlashCommand implements ISlashCommand {
  public String getDescription() {
    return "Pong!";
  }
  
  public void handle(SlashCommandInteractionEvent event) {
    event.deferReply().queue(interactionHook -> event.getJDA().getRestPing().queue(ping -> {
      EmbedBuilder eb = new EmbedBuilder()
              .setTitle("Pong !")
              .addField("Gateway Ping : ", String.format("%dms", event.getJDA().getGatewayPing()), false)
              .addField("Rest Ping : ", String.format("%sms", ping), false);
      interactionHook.sendMessageEmbeds(eb.build()).queue();
    }));
  }
}
