package fr.melaine_gerard.catboat.command;

import fr.melaine_gerard.catboat.command.music.JoinSlashCommand;
import fr.melaine_gerard.catboat.command.music.LeaveSlashCommand;
import fr.melaine_gerard.catboat.command.music.NowPlayingSlashCommand;
import fr.melaine_gerard.catboat.command.music.PlaySlashCommand;
import fr.melaine_gerard.catboat.command.music.QueueSlashCommand;
import fr.melaine_gerard.catboat.command.music.RepeatSlashCommand;
import fr.melaine_gerard.catboat.command.music.ShuffleSlashCommand;
import fr.melaine_gerard.catboat.command.music.SkipSlashCommand;
import fr.melaine_gerard.catboat.command.music.StopSlashCommand;
import fr.melaine_gerard.catboat.command.music.VolumeSlashCommand;
import fr.melaine_gerard.catboat.command.utils.PingSlashCommand;
import fr.melaine_gerard.catboat.core.BotCore;
import fr.melaine_gerard.catboat.utils.Constants;
import java.util.HashMap;
import java.util.Map;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.requests.restaction.CommandCreateAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommandManager {
  private final Map<String, ISlashCommand> commands;
  
  private static CommandManager instance;
  
  private final Logger logger = LoggerFactory.getLogger(CommandManager.class);
  
  public static CommandManager getInstance() {
    if (instance == null)
      instance = new CommandManager(); 
    return instance;
  }
  
  public CommandManager() {
    this.commands = new HashMap<>();
    addCommand(new PingSlashCommand());
    addCommand(new PlaySlashCommand());
    addCommand(new SkipSlashCommand());
    addCommand(new StopSlashCommand());
    addCommand(new JoinSlashCommand());
    addCommand(new LeaveSlashCommand());
    addCommand(new VolumeSlashCommand());
    addCommand(new NowPlayingSlashCommand());
    addCommand(new QueueSlashCommand());
    addCommand(new RepeatSlashCommand());
    addCommand(new ShuffleSlashCommand());
    registerCommands();
  }
  
  public void addCommand(ISlashCommand command) {
    if (this.commands.containsKey(command.getName()))
      throw new IllegalArgumentException("Command already exists"); 
    this.commands.put(command.getName(), command);
  }
  
  public ISlashCommand getCommand(String name) {
    return this.commands.get(name);
  }
  
  public boolean hasCommand(String name) {
    return this.commands.containsKey(name);
  }
  
  public void registerCommands() {
    this.commands.forEach((name, command) -> {
          this.logger.info("Registering command {}...", name);
          Guild guild = BotCore.getInstance().getJda().getGuildById(Constants.GUILD_ID);
          if (guild == null)
            return; 
          CommandCreateAction commandJDA = guild.upsertCommand(command.getName(), command.getDescription());
          if (!command.getOptions().isEmpty()) {
            commandJDA.addOptions(command.getOptions()).queue();
          } else {
            commandJDA.queue();
          } 
          this.logger.info("Command {} registered", name);
        });
  }
  
  public void handleCommand(SlashCommandInteractionEvent event) {
    String commandName = event.getName();
    logger.info("Command NAme: " + commandName);
    if (event.getMember() == null) {
      event.reply("Vous devez être connecté pour utiliser cette commande.").queue();
      return;
    } 
    if (event.getGuild() == null) {
      this.logger.info("received command {} from a private channel", commandName);
      event.reply("Tu dois utiliser les commandes sur un serveur !").queue();
      return;
    } 
    if (hasCommand(commandName)) {
      ISlashCommand slashCommand = getCommand(commandName);
      if (slashCommand.isOwnerCommand() && !event.getMember().getId().equals(Constants.BOT_OWNER)) {
        event.reply("Tu ne peux pas utiliser cette commande !").setEphemeral(true).queue();
        return;
      }

      if (!slashCommand.permissionsNeeded().isEmpty() && !event.getMember().hasPermission(slashCommand.permissionsNeeded())) {
        event.reply("Tu ne peux pas utiliser cette commande !").setEphemeral(true).queue();
        return;
      }


      try {
        slashCommand.handle(event);
      } catch (Exception e) {
        this.logger.error("Error while handling command", e);
        event.reply("Une erreur est survenue lors de l'exécution de la commande").setEphemeral(true).queue();
      } 
    } 
  }
}
