package fr.melaine_gerard.catboat.listener;

import net.dv8tion.jda.api.JDA;

public class ListenerManager {
  private final JDA jda;
  
  public ListenerManager(JDA jda) {
    this.jda = jda;
  }
  
  public void registerListeners() {
    this.jda.addEventListener(new SlashCommandListener());
    this.jda.addEventListener(new VoiceChannelListener());
  }
}
