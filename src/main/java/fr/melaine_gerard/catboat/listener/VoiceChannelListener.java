package fr.melaine_gerard.catboat.listener;

import fr.melaine_gerard.catboat.utils.MusicHelper;
import fr.melaine_gerard.catboat.utils.lavaplayer.PlayerManager;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceUpdateEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class VoiceChannelListener extends ListenerAdapter {
  @Override
  public void onGuildVoiceUpdate(GuildVoiceUpdateEvent event) {
    if (event.getChannelLeft() == null) return;
    if (event.getChannelLeft().getMembers().size() == 1 &&
            event.getChannelLeft().getMembers().contains(event.getGuild().getSelfMember())) {
      (PlayerManager.getInstance().getMusicManager(event.getGuild())).scheduler.repeating = false;
      (PlayerManager.getInstance().getMusicManager(event.getGuild())).scheduler.queue.clear();
      (PlayerManager.getInstance().getMusicManager(event.getGuild())).scheduler.player.stopTrack();
      MusicHelper.leaveChannel(event.getGuild().getAudioManager());
    }
  }
}
