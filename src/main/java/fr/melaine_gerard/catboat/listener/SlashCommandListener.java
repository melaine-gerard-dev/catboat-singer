package fr.melaine_gerard.catboat.listener;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.melaine_gerard.catboat.command.CommandManager;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class SlashCommandListener extends ListenerAdapter {
  private static final Logger logger = LoggerFactory.getLogger(SlashCommandListener.class);
  private final CommandManager commandManager = CommandManager.getInstance();
  
  public void onSlashCommandInteraction(@NotNull SlashCommandInteractionEvent event) {
    logger.info("Loading...");
    this.commandManager.handleCommand(event);
  }
}
