package fr.melaine_gerard.catboat.core;

import fr.melaine_gerard.catboat.utils.Constants;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.requests.GatewayIntent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BotCore {
  private static BotCore instance;
  
  private JDA jda;
  
  private static final Logger logger = LoggerFactory.getLogger(BotCore.class);
  
  public static BotCore getInstance() {
    if (instance == null)
      instance = new BotCore(); 
    return instance;
  }
  
  public BotCore() {
    try {
      this.jda = JDABuilder.createDefault(Constants.BOT_TOKEN).setEnabledIntents(GatewayIntent.getIntents(GatewayIntent.ALL_INTENTS)).build().awaitReady();
      this.jda.getPresence().setPresence(OnlineStatus.DO_NOT_DISTURB, Activity.playing(Constants.PLAYING_MESSAGE));
    } catch (InterruptedException|IllegalStateException e) {
      logger.error("Error while creating starting bot :", e);
      System.exit(1);
    } 
  }
  
  public JDA getJda() {
    return this.jda;
  }
}
