package fr.melaine_gerard.catboat;

import fr.melaine_gerard.catboat.core.BotCore;
import fr.melaine_gerard.catboat.listener.ListenerManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CatboatSinger {
  private static final Logger logger = LoggerFactory.getLogger(CatboatSinger.class);
  
  public static void main(String[] args) {
    logger.info("Starting bot...");
    BotCore core = BotCore.getInstance();
    (new ListenerManager(core.getJda())).registerListeners();
    logger.info(core.getJda().getSelfUser().getName() + " is ready!");
  }
}
